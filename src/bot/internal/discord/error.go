package discord

import (
	"time"

	"github.com/bwmarrin/discordgo"
)

// Error returns an embed that indicates that gobottas has encountered an error.
// This function exists so that error messages can be kept consistent across all
// modules.
func Error(title, desc string) *discordgo.MessageEmbed {
	e := NewEmbed().
		Title(title).
		Description(desc).
		Color(ColorError).
		Timestamp(time.Now())
	return e.Embed()
}
