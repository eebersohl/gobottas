package ergast

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
)

// WDCCurrent queries the Ergast API for the current WDC standings.  The context
// parameter should define how long you are willing to wait for a response from
// Ergast.
func WDCCurrent(ctx context.Context) ([]*Position, error) {
	return driverPositions(ctx, 0, 0)
}

// WCCCurrent queries the Ergast API for the current WCC standings.  The context
// param defines how long the function is willing to wait for a response.
func WCCCurrent(ctx context.Context) ([]*Position, error) {
	return constructorPositions(ctx, 0, 0)
}

// WDCHistorical queries the Ergast API for the standings during the season
// specified by season and after the race specified by race. The context param
// defines how long the function will wait for a response from Ergast.
func WDCHistorical(ctx context.Context, season, race int) ([]*Position, error) {
	return driverPositions(ctx, season, race)
}

// WCCHistorical queriest the Ergast API for the standings during the season
// specified by season and after the race specified by race.  The context param
// defines how long the function will wait for a response from Ergast.
func WCCHistorical(ctx context.Context, season, race int) ([]*Position, error) {
	return constructorPositions(ctx, season, race)
}

type driverPositionsJSON struct {
	MRData struct {
		StandingsTable struct {
			StandingsLists []struct {
				DriverStandings []struct {
					Position string `json:"position"`
					Points   string `json:"points"`
					Wins     string `json:"wins"`
					Driver   struct {
						GivenName  string `json:"givenName"`
						FamilyName string `json:"familyName"`
					} `json:"Driver"`
				} `json:"DriverStandings"`
			} `json:"StandingsLists"`
		} `json:"StandingsTable"`
	} `json:"MRData"`
}

type constructorPositionsJSON struct {
	MRData struct {
		StandingsTable struct {
			StandingsLists []struct {
				ConstructorStandings []struct {
					Position    string `json:"position"`
					Points      string `json:"points"`
					Wins        string `json:"wins"`
					Constructor struct {
						Name string `json:"name"`
					} `json:"Constructor"`
				} `json:"ConstructorStandings"`
			} `json:"StandingsLists"`
		} `json:"StandingsTable"`
	} `json:"MRData"`
}

// positionURL returns the URL on the ergast API according to the parameters
// provided.
//
// Params:
//
// - championship: must be either "driver" or "constructor", if it is neither,
//                 then the default "driver" is used.
//
// - season:       the year in which the F1 season took place (e.g. 2021).  0
//                 indicates the current season.
//
// - race:         the cardinal number of the race (e.g.: Australia is normally
//                 1). 0 indicates the end of season results (or current if the
//                 season is still ongoing).
//
// The function does not check for valid season and race numbers, so if they are
// incorrect an error will likely be returned downstream.
func positionURL(championship string, season, race int) string {
	// default to driver's championship
	if championship != "driver" && championship != "constructor" {
		championship = "driver"
	}

	// get results from the current season
	if season == 0 {
		return fmt.Sprintf("https://ergast.com/api/f1/current/%d/%sStandings.json", race, championship)
	}

	// get historical results
	return fmt.Sprintf("https://ergast.com/api/f1/%d/%d/%sStandings.json", season, race, championship)
}

// driverPositions queries the ergast API for driver positions.  Season and race
// follow the parameter rules outlined in positionURL above.  The Ergast API is
// not super fast, so use the context to determine how long you're willing to wait
// on a response.
func driverPositions(ctx context.Context, season, race int) ([]*Position, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, positionURL("driver", season, race), nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create a valid http request: %v", err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to send http request on default client: %v", err)
	}
	defer resp.Body.Close()

	var driverJSON driverPositionsJSON
	err = json.NewDecoder(resp.Body).Decode(&driverJSON)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json response: %v", err)
	}

	var positions []*Position
	// this only looks like a nested for loop.  It's easier and safer to write it
	// this way, despite the fact that there should never be more than one list
	for _, list := range driverJSON.MRData.StandingsTable.StandingsLists {
		for _, driver := range list.DriverStandings {
			pos, err := strconv.Atoi(driver.Position)
			if err != nil {
				return nil, err
			}

			pts, err := strconv.ParseFloat(driver.Points, 64)
			if err != nil {
				return nil, err
			}

			wins, err := strconv.Atoi(driver.Wins)
			if err != nil {
				return nil, err
			}

			positions = append(positions, &Position{
				Points:   pts,
				Position: pos,
				Wins:     wins,
				Name:     fmt.Sprintf("%s %s", driver.Driver.GivenName, driver.Driver.FamilyName),
			})
		}
	}

	return positions, nil
}

// constructorPositions queries the ergast API for constructor positions.
// Season and race follow the parameter rules outlined in positionURL above.  The
// Ergast API is not super fast, so use the context to determine how long you're
// willing to wait on a response.
func constructorPositions(ctx context.Context, season, race int) ([]*Position, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, positionURL("constructor", season, race), nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create a valid http request: %v", err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to send http request on default client: %v", err)
	}
	defer resp.Body.Close()

	var constructorJSON constructorPositionsJSON
	err = json.NewDecoder(resp.Body).Decode(&constructorJSON)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json response: %v", err)
	}

	var positions []*Position
	// this only looks like a nested for loop.  It's easier and safer to write it
	// this way, despite the fact that there should never be more than one list
	for _, list := range constructorJSON.MRData.StandingsTable.StandingsLists {
		for _, constructor := range list.ConstructorStandings {
			pos, err := strconv.Atoi(constructor.Position)
			if err != nil {
				return nil, err
			}

			pts, err := strconv.ParseFloat(constructor.Points, 64)
			if err != nil {
				return nil, err
			}

			wins, err := strconv.Atoi(constructor.Wins)
			if err != nil {
				return nil, err
			}

			positions = append(positions, &Position{
				Points:   pts,
				Position: pos,
				Wins:     wins,
				Name:     constructor.Constructor.Name,
			})
		}
	}

	return positions, nil
}
