package core

import "testing"

func TestNewMessage(t *testing.T) {
	msg := NewMessage()
	if msg.Response == nil {
		t.Errorf("got nil response from NewMessage()")
	}

	if msg.Source == nil {
		t.Errorf("got nil source from NewMessage()")
	}
}
