package repo

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/eebersohl/gobottas/src/bot/internal/core"
	"gitlab.com/eebersohl/gobottas/src/bot/internal/discord"
	"gitlab.com/eebersohl/gobottas/src/log"
)

const (
	_gobottasRepo = "https://gitlab.com/eebersohl/gobottas"
)

// repo implements the core.Module interface
type repo struct {
	cmd       string
	aliases   []string
	projectId string
	logger    log.Logger
}

// allAliases returns a string slice containing all of the commands to which
// this module will respond.
func (r *repo) allAliases() []string {
	return append(r.aliases, r.cmd)
}

// NewRepo creates a repo with the properly initialized cmd and aliases fields
func NewRepo(projectId string, logger log.Logger) core.Module {
	return &repo{
		cmd:       "repo",
		aliases:   []string{"repository"},
		projectId: projectId,
		logger:    logger,
	}
}

// Name returns the name of the module.  Name is principally used for debugging.
func (r *repo) Name() string {
	return r.cmd
}

func (r *repo) Usage() string {
	return strings.Replace(_repoShort, "\n", "", -1)
}

// Intercept is run on all messages that gobottas sees.  If the command in the msg
// is relevant, this function will edit the response accordingly.
func (r *repo) Intercept(msg *core.Message) error {
	r.logger.Debugf("[%s] beginning interception", r.Name())

	if msg == nil {
		return fmt.Errorf("[%s] cannot intercept a nil message", r.Name())
	}

	if msg.Source == nil {
		return fmt.Errorf("[%s] cannot answer a message with nil source", r.Name())
	}

	if msg.Source.ChannelId == 0 {
		return fmt.Errorf("[%s] cannot respond to a message with an invalid channel id (cid = %d)", r.Name(), msg.Source.ChannelId)
	}

	var intercept bool
	for _, a := range r.allAliases() {
		if a == msg.Command {
			intercept = true
		}
	}

	// exit asap if not relevant to this module
	if !intercept {
		r.logger.Debugf("[%s] ending interception (irrelevant message)", r.Name(), msg.Command)
		return nil
	}
	r.logger.Debugf("[%s] module detected relevant message (command = %s)", r.Name(), msg.Command)

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	tag, err := GitTag(ctx, r.projectId)
	if err != nil {
		r.logger.Errorw("failed to query gitlab tags endpoint", "err", err)
	}

	commit, err := GitCommit(ctx, r.projectId)
	if err != nil {
		r.logger.Errorw("failed to query gitlab commits endpoint", "err", err)
	}

	e := discord.NewEmbed().
		Title("Gobottas is on gitlab").
		Description("The gobottas repository can be found here:\n"+_gobottasRepo).
		Color(discord.ColorHelp).
		Footer("Written by Eric Ebersohl", "", "")

	if tag != nil {
		tagTime := tag.Commit.Date
		if t, err := time.Parse(time.RFC3339, tag.Commit.Date); err == nil {
			tagTime = t.Format("02 January 2006")
		}

		e.AddField("Latest Version", fmt.Sprintf("The latest version is **%s**.  It was tagged on %s", tag.Name, tagTime))
	}

	if commit != nil {
		cTime := commit.Date
		if t, err := time.Parse(time.RFC3339, commit.Date); err == nil {
			cTime = t.Format("02 January 2006")
		}

		e.AddField("Latest Commit", fmt.Sprintf("The latest commit (short) is **%s**.\nIt was committed on **%s**.\nIts author is **%s**.",
			commit.ShortId, cTime, commit.Author))
	}

	msg.Response = &core.Response{
		ChannelId: msg.Source.ChannelId,
		Embed:     []*discordgo.MessageEmbed{e.Embed()},
		Text:      []string{_gobottasRepo},
	}

	r.logger.Debugf("[%s] ending interception", r.Name())
	return nil
}
