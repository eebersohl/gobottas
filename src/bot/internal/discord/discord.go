package discord

import (
	"fmt"
	"strconv"
)

// Snowflake is the unique identifier used by Discord (and other big platforms)
type Snowflake uint64

// String returns a string representation of a Snowflake
func (s Snowflake) String() string {
	return strconv.FormatUint(uint64(s), 10)
}

// ParseSnowflake attempts to parse a string into a Snowflake.  An error is
// returned if the operation fails.
func ParseSnowflake(s string) (Snowflake, error) {
	sf, err := strconv.ParseUint(s, 10, 64)
	if err != nil {
		return 0, fmt.Errorf("failed to parse string to uint64: %v", err)
	}

	return Snowflake(sf), nil
}
