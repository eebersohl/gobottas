package core

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/eebersohl/gobottas/src/bot/internal/discord"
)

// A Message is parsed by the registry from a discordgo.Message.  Every
// interceptor in the registry is called on every message that gets parsed,
// usually resulting in a no-op.
type Message struct {
	// Information about the message source
	Source *Source
	// If the first character of the message is the command prefix, then
	// this is set as the first token after the prefix.  Otherwise, it is
	// empty.  An empty command signifies that this is not a bot command
	// message
	Command string
	// additional arguments after the command
	Args []string
	// the response that is sent after all the interceptors have completed
	Response *Response
}

// NewMessage returns an empty message with all struct pointers initialized to
// their zero values.  Optionally include default help message embed(s) via
// parameters.
func NewMessage() *Message {
	return &Message{
		Source:   &Source{},
		Command:  "",
		Args:     nil,
		Response: &Response{},
	}
}

// Source contains information about the source that triggered a message
type Source struct {
	// unique id of the author
	AuthorId discord.Snowflake
	// human-readable author name (not including number)
	Author string
	// unique id of the channel
	ChannelId discord.Snowflake
	// string content of the original message
	Content string
}

// Response contains the response to a message that warrants feedback
type Response struct {
	// the channel where the response should be sent
	ChannelId discord.Snowflake
	// An embedded message(s) to send on the channel
	// If this message is not nil, the embed will be sent and the Text field
	// will be ignored
	Embed []*discordgo.MessageEmbed
	// Text message(s) to be sent on the channel
	// This field is only used if Embed is nil
	Text []string
}
