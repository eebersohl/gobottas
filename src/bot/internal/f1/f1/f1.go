package f1

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/eebersohl/gobottas/src/bot/internal/core"
	"gitlab.com/eebersohl/gobottas/src/bot/internal/discord"
	"gitlab.com/eebersohl/gobottas/src/bot/internal/f1/ergast"
	"gitlab.com/eebersohl/gobottas/src/log"
)

// the f1 module adds a discord text channel interface for querying the Ergast
// API for f1 standings and results.
type f1 struct {
	cmd     string
	aliases []string
	subs    []core.Module
	logger  log.Logger
}

// allAliases returns a slice of all aliases for the module
func (f *f1) allAliases() []string {
	return append([]string{f.cmd}, f.aliases...)
}

// NewF1 creates a new core.Module that implements the f1 functionality.  The
// f1 submodules are added automatically.
func NewF1(logger log.Logger) core.Module {
	f := &f1{
		cmd:     "f1",
		aliases: []string{},
		subs: []core.Module{
			NewStandings(logger),
			//NewResults(logger),
		},
		logger: logger,
	}

	fields := make(map[string]string)
	for _, mod := range f.subs {
		fields[mod.Name()] = mod.Usage()
	}

	f.subs = append(f.subs, core.NewHelp(fields, _f1Long, _f1Short, false, true, logger))

	return f
}

// Name returns the name of the module
func (f *f1) Name() string {
	return f.cmd
}

// Usage returns a short explanation of the command's function
func (f *f1) Usage() string {
	return strings.Replace(_f1Short, "\n", " ", -1)
}

// Intercept is called on every message that Gobottas sees.  If the parsed command
// is identical to one of the command's aliases, the message response will be set
// according to the module's function.
func (f *f1) Intercept(msg *core.Message) error {
	f.logger.Debugf("[%s] beginning interception", f.Name())

	if msg == nil {
		return fmt.Errorf("[%s] cannot intercept a nil message", f.Name())
	}

	if msg.Source == nil {
		return fmt.Errorf("[%s] cannot answer a message with nil source", f.Name())
	}

	if msg.Source.ChannelId == 0 {
		return fmt.Errorf("[%s] cannot respond to a message with an invalid channel id (cid = %d)", f.Name(), msg.Source.ChannelId)
	}

	var intercept bool
	for _, a := range f.allAliases() {
		if a == msg.Command {
			intercept = true
		}
	}

	// exit asap if not relevant to this module
	if !intercept {
		return nil
	}

	f.logger.Debugf("[%s] module detected a relevant message (command = %s)", f.Name(), msg.Command)

	msg.Response.ChannelId = msg.Source.ChannelId

	// if there are args, send the message to the subcommands
	if len(msg.Args) > 0 {
		for _, sub := range f.subs {
			err := sub.Intercept(msg)
			if err != nil {
				return err
			}
		}
		return nil
	}

	// if there are no args, add the default embed
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	ds, err := ergast.WDCCurrent(ctx)
	if err != nil {
		return err
	}

	cs, err := ergast.WCCCurrent(ctx)
	if err != nil {
		return err
	}

	drivers := discord.NewEmbed().
		Title("F1 Driver's Standings").
		Color(discord.ColorF1)

	for i := 0; i < 3; i++ {
		drivers.AddField(
			fmt.Sprintf("%02d %s", ds[i].Position, ds[i].Name),
			fmt.Sprintf("%.1f points, %d wins", ds[i].Points, ds[i].Wins),
		)
	}

	constructors := discord.NewEmbed().
		Title("F1 Constructor's Standings").
		Color(discord.ColorF1)

	for i := 0; i < 3; i++ {
		constructors.AddField(
			fmt.Sprintf("%02d %s", cs[i].Position, cs[i].Name),
			fmt.Sprintf("%.1f points, %d wins", cs[i].Points, ds[i].Wins),
		)
	}

	msg.Response.Embed = []*discordgo.MessageEmbed{drivers.Embed(), constructors.Embed()}

	f.logger.Debugf("[%s] ending interception", f.Name())
	return nil
}
