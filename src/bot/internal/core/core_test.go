package core

import (
	"context"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"github.com/google/go-cmp/cmp"
	"gitlab.com/eebersohl/gobottas/src/bot/internal/discord"
	"testing"
	"time"
)

/*
Test Cases:
- nil string
- start quotes, end quotes
- single quote with whitespace
*/
func TestTokenize(t *testing.T) {
	tests := []struct {
		name    string
		in      string
		out     []string
		wantErr bool
	}{
		{name: "nil", in: "", out: nil, wantErr: false},
		{name: "start-quotation", in: `"start quotes" and other args`, out: []string{"start quotes", "and", "other", "args"}, wantErr: false},
		{name: "end-quotation", in: `other args "end quotes"`, out: []string{"other", "args", "end quotes"}, wantErr: false},
		{name: "single-quote", in: `a " b`, out: []string{"a", "b"}, wantErr: false},
		{name: "tabs", in: "a\tb\tc", out: []string{"a", "b", "c"}, wantErr: false},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			got, err := tokenize(test.in)
			if (err != nil) != test.wantErr {
				t.Errorf("err != wantErr (err = %v, wantErr = %v)", err, test.wantErr)
			}

			if !cmp.Equal(test.out, got) {
				t.Errorf("out != got (%s)", cmp.Diff(test.out, got))
			}
		})
	}
}

/*
Test Cases:
- message is from a bot
- no command message
- command message
*/
func TestRegistry_Handler(t *testing.T) {
	tests := []struct {
		name    string
		m       *discordgo.MessageCreate
		wantMsg bool // actual message content is tested elsewhere
	}{
		{name: "bot author", m: &discordgo.MessageCreate{
			Message: &discordgo.Message{
				ChannelID: "1",
				Content:   "beep boop",
				Author: &discordgo.User{
					ID:       "1",
					Username: "bot",
					Bot:      true,
				},
			},
		}, wantMsg: false},
		{name: "message no command", m: &discordgo.MessageCreate{
			Message: &discordgo.Message{
				ChannelID: "1",
				Content:   "this is not a command",
				Author: &discordgo.User{
					ID:       "1",
					Username: "real human",
					Bot:      false,
				},
			},
		}, wantMsg: true},
		{name: "message command", m: &discordgo.MessageCreate{
			Message: &discordgo.Message{
				ChannelID: "1",
				Content:   "&help",
				Author: &discordgo.User{
					ID:       "1",
					Username: "real human",
					Bot:      false,
				},
			},
		}, wantMsg: true},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ch := make(chan *Message)
			defer close(ch)
			r, _ := NewRegistry()
			fn := r.Handler(ch)
			ctx, cancel := context.WithTimeout(context.Background(), time.Second)
			defer cancel()

			var result *Message
			go func(ctx context.Context) {
				select {
				case result = <-ch:
					return
				case <-ctx.Done():
					return
				}
			}(ctx)

			fn(nil, test.m)

			<-ctx.Done()

			if (result != nil) != test.wantMsg {
				t.Errorf("got msg != want msg (got = %v, want = %t)", result, test.wantMsg)
			}
		})
	}
}

/*
Test Cases:
- default
- withModules
- withPrefix
- withLocalDir
*/
func TestRegistry_Opts(t *testing.T) {
	tests := []struct {
		name        string
		opts        []RegistryOpt
		want        *Registry
		wantModules int
	}{
		{name: "default", opts: []RegistryOpt{}, want: &Registry{
			modules:       []Module{},
			local:         ".",
			commandPrefix: '&',
		}, wantModules: 1},
		{name: "with modules", opts: []RegistryOpt{WithModules(&moduleA{}, &moduleB{})}, want: &Registry{
			modules:       []Module{},
			local:         ".",
			commandPrefix: '&',
		}, wantModules: 3},
		{name: "with prefix", opts: []RegistryOpt{WithCommandPrefix('!')}, want: &Registry{
			modules:       []Module{},
			local:         ".",
			commandPrefix: '!',
		}, wantModules: 1},
		{name: "with local dir", opts: []RegistryOpt{WithLocalDir("/tmp/gobottas")}, want: &Registry{
			modules:       []Module{},
			local:         "/tmp/gobottas",
			commandPrefix: '&',
		}, wantModules: 1},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			got, err := NewRegistry(test.opts...)
			if err != nil {
				t.Fatal(err)
			}

			// can't use go-cmp on the whole struct due to unexported fields
			if test.wantModules != len(got.modules) {
				t.Errorf("got modules len != want modules len (got = %d, want = %d)", len(got.modules), test.wantModules)
			}

			if got.local != test.want.local {
				t.Errorf("got local != want local (got = %s, want = %s)", got.local, test.want.local)
			}

			if got.commandPrefix != test.want.commandPrefix {
				t.Errorf("got prefix != want prefix (got = %s, want = %s)", string(got.commandPrefix), string(test.want.commandPrefix))
			}
		})
	}
}

/*
Test Cases:
- zero modules
- two modules
*/
func TestRegistry_Modules(t *testing.T) {
	zero, err := NewRegistry()
	if err != nil {
		t.Fatal(err)
	}

	two, err := NewRegistry(WithModules(&moduleA{}, &moduleB{}))
	if err != nil {
		t.Fatal(err)
	}

	tests := []struct {
		name     string
		registry *Registry
		want     int
	}{
		{name: "zero", registry: zero, want: 1},
		{name: "two", registry: two, want: 3},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			got := test.registry.Modules()
			if len(got) != test.want {
				t.Errorf("got len != want len (got = %d, want = %d)", len(got), test.want)
			}
		})
	}
}

/*
Test Cases:
- nil msg
- msg with nil response
- no interceptors
- interceptors
*/
func TestRegistry_RunInterceptors(t *testing.T) {
	zero, err := NewRegistry()
	if err != nil {
		t.Fatal(err)
	}

	a, err := NewRegistry(WithModules(&moduleA{}))
	if err != nil {
		t.Fatal(err)
	}

	ab, err := NewRegistry(WithModules(&moduleA{}, &moduleB{}))
	if err != nil {
		t.Fatal(err)
	}

	cId, _ := discord.ParseSnowflake("175928847299117063")

	tests := []struct {
		name         string
		msg          *Message
		registry     *Registry
		wantRespText []string
		wantErr      bool
	}{
		{name: "nil msg", msg: nil, registry: zero, wantRespText: nil, wantErr: true},
		{name: "nil resp", msg: &Message{}, registry: a, wantRespText: nil, wantErr: true},
		{name: "no interceptors", msg: &Message{Source: &Source{ChannelId: cId}}, registry: zero, wantRespText: nil, wantErr: false},
		{name: "interceptors", msg: &Message{Source: &Source{ChannelId: cId}}, registry: ab, wantRespText: []string{"moduleA", "moduleB"}, wantErr: false},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			err := test.registry.runInterceptors(test.msg)
			if (err != nil) != test.wantErr {
				t.Errorf("(err != nil) != test.wantErr (err = %v, test.wantErr = %t)", err, test.wantErr)
			}

			if err == nil && !test.wantErr {
				if eq := cmp.Diff(test.msg.Response.Text, test.wantRespText); eq != "" {
					t.Errorf("got msg text != want msg text\n%s", eq)
				}
			}
		})
	}
}

/*
Test Cases:
- nil in
- nil author
- invalid author snowflake
- invalid channel snowflake
- command and args
- command no args
- not command
*/
func TestRegistry_ParseMessage(t *testing.T) {
	zero, err := NewRegistry()
	if err != nil {
		t.Fatal(err)
	}

	tests := []struct {
		name     string
		registry *Registry
		in       *discordgo.Message
		out      *Message
		wantErr  bool
	}{
		{name: "nil in", registry: zero, in: nil, out: nil, wantErr: true},
		{name: "nil author", registry: zero, in: &discordgo.Message{Author: nil}, out: nil, wantErr: true},
		{name: "invalid author snowflake", registry: zero, in: &discordgo.Message{
			Author: &discordgo.User{
				ID: "notasnowflake",
			},
		}, out: nil, wantErr: true},
		{name: "invalid channel snowflake", registry: zero, in: &discordgo.Message{
			Author: &discordgo.User{
				ID:       "1",
				Username: "test-username",
			},
			ChannelID: "notasnowflake",
		}, out: nil, wantErr: true},
		{name: "command and args", registry: zero, in: &discordgo.Message{
			Author: &discordgo.User{
				ID:       "1",
				Username: "test-username",
			},
			ChannelID: "1",
			Content:   `&command arg1 arg2 arg3 "compound arg 4"`,
		}, out: &Message{
			Source: &Source{
				AuthorId:  1,
				Author:    "test-username",
				ChannelId: 1,
				Content:   `&command arg1 arg2 arg3 "compound arg 4"`,
			},
			Command:  "command",
			Args:     []string{"arg1", "arg2", "arg3", "compound arg 4"},
			Response: &Response{},
		}},
		{name: "command no args", registry: zero, in: &discordgo.Message{
			Author: &discordgo.User{
				ID:       "1",
				Username: "test-username",
			},
			ChannelID: "1",
			Content:   `&command`,
		}, out: &Message{
			Source: &Source{
				AuthorId:  1,
				Author:    "test-username",
				ChannelId: 1,
				Content:   `&command`,
			},
			Command:  "command",
			Args:     []string{},
			Response: &Response{},
		}},
		{name: "no command no args", registry: zero, in: &discordgo.Message{
			Author: &discordgo.User{
				ID:       "1",
				Username: "test-username",
			},
			ChannelID: "1",
			Content:   `this is not a command`,
		}, out: &Message{
			Source: &Source{
				AuthorId:  1,
				Author:    "test-username",
				ChannelId: 1,
				Content:   `this is not a command`,
			},
			Command:  "",
			Args:     nil,
			Response: &Response{},
		}},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			got, err := test.registry.parseMessage(test.in)
			if (err != nil) != test.wantErr {
				t.Errorf("(err != nil) != test.wantErr (err = %v, test.wantErr = %t)", err, test.wantErr)
			}

			if err == nil && !test.wantErr {
				if eq := cmp.Diff(got, test.out); eq != "" {
					t.Errorf("got msg != want msg:\n%s", eq)
				}
			}
		})
	}
}

type moduleA struct{}

func (m *moduleA) Name() string {
	return "moduleA"
}

func (m *moduleA) Usage() string {
	return "modA usage"
}

func (m *moduleA) Intercept(msg *Message) error {
	if msg == nil || msg.Source == nil {
		return fmt.Errorf("msg and/or msg.Source are not defined")
	}

	if msg.Response == nil {
		msg.Response = &Response{ChannelId: msg.Source.ChannelId}
	}

	msg.Response.Text = append(msg.Response.Text, "moduleA")
	return nil
}

type moduleB struct{}

func (m *moduleB) Name() string {
	return "moduleB"
}

func (m *moduleB) Usage() string {
	return "modB usage"
}

func (m *moduleB) Intercept(msg *Message) error {
	if msg == nil || msg.Source == nil {
		return fmt.Errorf("msg and/or msg.Source are not defined")
	}

	if msg.Response == nil {
		msg.Response = &Response{ChannelId: msg.Source.ChannelId}
	}

	msg.Response.Text = append(msg.Response.Text, "moduleB")
	return nil
}
