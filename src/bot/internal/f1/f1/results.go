package f1

import (
	"gitlab.com/eebersohl/gobottas/src/bot/internal/core"
	"gitlab.com/eebersohl/gobottas/src/log"
	"strings"
)

type results struct {
	cmd     string
	aliases []string
	logger  log.Logger
}

func (r *results) allAliases() []string {
	return append(r.aliases, r.cmd)
}

func NewResults(logger log.Logger) core.Module {
	return &results{
		cmd:     "results",
		aliases: []string{},
		logger:  logger,
	}
}

func (r *results) Name() string {
	return r.cmd
}

func (r *results) Usage() string {
	return strings.Replace(_resultsShort, "\n", "", -1)
}

func (r *results) Intercept(msg *core.Message) error {
	panic("implement me")
}
