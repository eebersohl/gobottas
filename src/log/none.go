package log

import "os"

// None implements the log.Logger interface with noops that don't panic
type None struct{}

func (n *None) Info(args ...interface{}) {
	// do nothing
}

func (n *None) Debug(args ...interface{}) {
	// do nothing
}

func (n *None) Error(args ...interface{}) {
	// do nothing
}

func (n *None) Fatal(args ...interface{}) {
	os.Exit(1)
}

func (n *None) Infof(template string, args ...interface{}) {
	// do nothing
}

func (n *None) Debugf(template string, args ...interface{}) {
	// do nothing
}

func (n *None) Errorf(template string, args ...interface{}) {
	// do nothing
}

func (n *None) Fatalf(template string, args ...interface{}) {
	os.Exit(1)
}

func (n *None) Infow(msg string, keysAndValues ...interface{}) {
	// do nothing
}

func (n *None) Debugw(msg string, keysAndValues ...interface{}) {
	// do nothing
}

func (n *None) Errorw(msg string, keysAndValues ...interface{}) {
	// do nothing
}

func (n *None) Fatalw(msg string, keysAndValues ...interface{}) {
	os.Exit(1)
}

func (n *None) With(keysAndValues ...interface{}) Logger {
	return n
}
