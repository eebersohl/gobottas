package core

import (
	"strings"
	"testing"

	"gitlab.com/eebersohl/gobottas/src/log"
)

/*
Test Cases:
- nil fields
- non nil fields
*/
func TestNewHelp(t *testing.T) {
	tests := []struct {
		name       string
		fields     map[string]string
		wantEmbed  bool
		wantFields int
		wantTitle  string
	}{
		{name: "nil fields", fields: nil, wantEmbed: true, wantFields: 1, wantTitle: "Gobottas usage instructions"},
		{name: "non nil fields", fields: map[string]string{"a": "a usage", "b": "b usage"}, wantEmbed: true, wantFields: 3, wantTitle: "Gobottas usage instructions"},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			mod := NewHelp(test.fields, _long, _short, true, true, &log.None{})

			h, ok := mod.(*help)
			if !ok {
				t.Errorf("expected help got not help (somehow)")
			}

			if (h.embed != nil) != test.wantEmbed {
				t.Errorf("got embed != want embed (got = %v, want = %t)", h.embed, test.wantEmbed)
			}

			if h.embed != nil {
				if len(h.embed.Fields) != test.wantFields {
					t.Errorf("got num fields != test want num fields (got = %d, want = %d)", len(h.embed.Fields), test.wantFields)
				}

				if h.embed.Title != test.wantTitle {
					t.Errorf("got title != want title (got = %s, want = %s)", h.embed.Title, test.wantTitle)
				}
			}
		})
	}
}

func TestHelp_Name(t *testing.T) {
	h := NewHelp(nil, _long, _short, true, true, &log.None{})

	if h.Name() != "help" {
		t.Errorf("got name != want name (got = %s, want = %s)", h.Name(), "help")
	}

	if h.Usage() != strings.Replace(_short, "\n", " ", -1) {
		t.Errorf("got usage != want usage (got = %s, want = %s)", h.Usage(), strings.Replace(_short, "\n", " ", -1))
	}
}

/*
Test Cases:
- nil msg
- nil src
- invalid channel id
- not relevant
- relevant
*/
func TestHelp_Intercept(t *testing.T) {
	tests := []struct {
		name      string
		in        *Message
		wantEmbed bool
		wantErr   bool
	}{
		{name: "nil msg", in: nil, wantEmbed: false, wantErr: true},
		{name: "nil source", in: &Message{}, wantEmbed: false, wantErr: true},
		{name: "invalid channel id", in: &Message{Source: &Source{ChannelId: 0}}, wantEmbed: false, wantErr: true},
		{name: "irrelevant", in: &Message{Source: &Source{ChannelId: 1}, Command: "cmd", Args: []string{"arg", "arg2"}, Response: &Response{}}, wantEmbed: false, wantErr: false},
		{name: "relevant cmd", in: &Message{Source: &Source{ChannelId: 1}, Command: "help", Response: &Response{}}, wantEmbed: true, wantErr: false},
		{name: "relevant arg", in: &Message{Source: &Source{ChannelId: 1}, Command: "not-help", Args: []string{"ok", "maybe", "help"}, Response: &Response{}}, wantEmbed: true, wantErr: false},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			h := NewHelp(nil, _long, _short, true, true, &log.None{})
			err := h.Intercept(test.in)
			if (err != nil) != test.wantErr {
				t.Errorf("bad err state (err = %v, wantErr = %t)", err, test.wantErr)
			}

			if err == nil && !test.wantErr {
				if (test.in.Response.Embed != nil) != test.wantEmbed {
					t.Errorf("bad embed state (embed = %v, wantEmbed = %t)", test.in.Response.Embed, test.wantEmbed)
				}
			}
		})
	}
}
