# Pi Gitlab Runner Installation (Ansible)

1. Install Ansible
2. Make a `hosts.yml` file based on the example_hosts.yml file
3. Run the playbook: `ansible-playbook -i hosts.yml provision.yml --ask-pass`