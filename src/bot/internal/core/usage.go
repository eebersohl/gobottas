package core

// usage.go contains the text that gobottas sends as part of the help embed
// the strings have newlines here for easy reading, but the newlines are stripped
// before sending to discord where discord will handle the formatting.

const (
	_long = `Gobottas is a discord bot that responds to chat commands in any
channel that the server administrator allows it to see. While Gobottas sees
every message that gets sent, only messages whose first character is the
"command prefix" will be acknowledged responded to. The default command prefix
is '&', but the administrator of the bot has the power to change that character
if they wish. Use the command **&help** to see what kinds of things Gobottas can
do.`

	_short = `Returns information about how to use Gobottas.`
)
