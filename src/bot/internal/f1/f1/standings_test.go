package f1

import (
	"github.com/google/go-cmp/cmp"
	"gitlab.com/eebersohl/gobottas/src/bot/internal/core"
	"gitlab.com/eebersohl/gobottas/src/log"
	"strings"
	"testing"
)

func TestStandings_Name(t *testing.T) {
	s := NewStandings(&log.None{})

	if got := s.Name(); got != "standings" {
		t.Errorf("got name != want name (got = %s, want = %s)", got, "standings")
	}
}

func TestStandings_Usage(t *testing.T) {
	s := NewStandings(&log.None{})
	want := strings.Replace(_standingsShort, "\n", " ", -1)

	if got := s.Usage(); got != want {
		t.Errorf("got usage != want usage (got = %s, want = %s)", got, want)
	}
}

/*
Test Cases:
- no args
- not relevant
*/
func TestStandings_Intercept(t *testing.T) {
	tests := []struct {
		name     string
		in       *core.Message
		wantErr  bool
		wantResp *core.Response
	}{
		{name: "no args", in: core.NewMessage(), wantErr: false, wantResp: &core.Response{}},
		{name: "not relevant", in: &core.Message{
			Source:   nil,
			Command:  "",
			Args:     []string{"results", "2012", "1"},
			Response: &core.Response{},
		}, wantErr: false, wantResp: &core.Response{}},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			s := NewStandings(&log.None{})
			err := s.Intercept(test.in)
			if (err != nil) != test.wantErr {
				t.Errorf("bad err state (err = %v, wantErr = %t)", err, test.wantErr)
			}

			if err == nil && !test.wantErr {
				if eq := cmp.Diff(test.in.Response, test.wantResp); eq != "" {
					t.Error(eq)
				}
			}
		})
	}
}

/*
Test Cases:
- zero args (nil / empty slice)

- one arg invalid string
- one arg invalid year
- one arg valid year
- one arg valid championship

- two args year/race invalid strings
- two args year/race invalid year
- two args year/race valid

- two args championship/year invalid strings
- two args championship/year invalid year
- two args championship/year valid

- three args invalid strings
- three args invalid year
- three args invalid race
- three args valid

This is overkill on test cases, but once the basic structure is implemented,
adding more tests is trivial.
*/
func TestParseArgs(t *testing.T) {
	tests := []struct {
		name             string
		args             []string
		wantChampionship string
		wantYear         int
		wantRace         int
		wantErr          bool
	}{
		{name: "zero args nil", args: nil, wantChampionship: "both", wantYear: 0, wantRace: 0, wantErr: false},
		{name: "zero args slice", args: []string{}, wantChampionship: "both", wantYear: 0, wantRace: 0, wantErr: false},

		{name: "one arg invalid string", args: []string{"photo"}, wantChampionship: "", wantYear: 0, wantRace: 0, wantErr: true},
		{name: "one arg year invalid year", args: []string{"1932"}, wantChampionship: "", wantYear: 0, wantRace: 0, wantErr: true},
		{name: "one arg year valid", args: []string{"2021"}, wantChampionship: "both", wantYear: 2021, wantRace: 0, wantErr: false},
		{name: "one arg championship valid", args: []string{"wdc"}, wantChampionship: "drivers", wantYear: 0, wantRace: 0, wantErr: false},

		{name: "two args first invalid string", args: []string{"version", "3"}, wantChampionship: "", wantYear: 0, wantRace: 0, wantErr: true},
		{name: "two args second invalid string", args: []string{"2012", "map"}, wantChampionship: "", wantYear: 0, wantRace: 0, wantErr: true},
		{name: "two args invalid year", args: []string{"1949", "6"}, wantChampionship: "", wantYear: 0, wantRace: 0, wantErr: true},
		{name: "two args valid", args: []string{"2012", "13"}, wantChampionship: "both", wantYear: 2012, wantRace: 13, wantErr: false},

		{name: "two args invalid string 1", args: []string{"anxiety", "1965"}, wantChampionship: "", wantYear: 0, wantRace: 0, wantErr: true},
		{name: "two args invalid string 2", args: []string{"constructors", "society"}, wantChampionship: "", wantYear: 0, wantRace: 0, wantErr: true},
		{name: "two args invalid year", args: []string{"constructors", "1948"}, wantChampionship: "", wantYear: 0, wantRace: 0, wantErr: true},
		{name: "two args valid", args: []string{"constructors", "1965"}, wantChampionship: "constructors", wantYear: 1965, wantRace: 0, wantErr: false},

		{name: "three args invalid strings 1", args: []string{"writing", "1977", "5"}, wantChampionship: "", wantYear: 0, wantRace: 0, wantErr: true},
		{name: "three args invalid strings 2", args: []string{"wcc", "client", "5"}, wantChampionship: "", wantYear: 0, wantRace: 0, wantErr: true},
		{name: "three args invalid strings 3", args: []string{"wcc", "1977", "priority"}, wantChampionship: "", wantYear: 0, wantRace: 0, wantErr: true},
		{name: "three args invalid year", args: []string{"wcc", "1935", "5"}, wantChampionship: "", wantYear: 0, wantRace: 0, wantErr: true},
		{name: "three args invalid race", args: []string{"wcc", "1977", "-1"}, wantChampionship: "", wantYear: 0, wantRace: 0, wantErr: true},
		{name: "three args valid", args: []string{"wcc", "1977", "5"}, wantChampionship: "constructors", wantYear: 1977, wantRace: 5, wantErr: false},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			championship, year, race, err := parseArgs(test.args)
			if (err != nil) != test.wantErr {
				t.Errorf("bad err state (err = %v, wantErr = %t)", err, test.wantErr)
			}

			if err == nil && !test.wantErr {
				if championship != test.wantChampionship {
					t.Errorf("got championship != want championship (got = %s, want = %s)", championship, test.wantChampionship)
				}

				if year != test.wantYear {
					t.Errorf("got year != want year (got = %d, want = %d)", year, test.wantYear)
				}

				if race != test.wantRace {
					t.Errorf("got race != want race (got = %d, want = %d)", race, test.wantRace)
				}
			}
		})
	}
}
