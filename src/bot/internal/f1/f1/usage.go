package f1

// usage.go contains usage strings for the f1 modules.  In order for the best end
// result, all newlines are replaced with empty strings before being sent to
// discord.  Some newlines are meant to be preserved, however, so the tag <br>
// will be replaced with a newline character.

const (
	_f1Short = `The f1 command returns results and standings from current and
historical Formula One races.`

	_f1Long = `The f1 command returns results and standings from current and
historical Formula One races. Use the **standings** subcommand to get the
standings at a particular point in time.  Use the **results** subcommand to get
results from a particular event.`

	_standingsShort = `The standings subcommand returns the standings in the WDC or WCC after a given race`
	_standingsLong  = _f1Long

	_resultsShort = `The results subcommand returns results from particular Formula One races.`
	_resultsLong  = _f1Long
)
