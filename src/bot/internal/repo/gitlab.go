package repo

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

type Tag struct {
	Name   string `json:"name"`
	Commit struct {
		Date string `json:"committed_date"`
	}
}

func GitTag(ctx context.Context, projectId string) (*Tag, error) {
	if projectId == "" {
		return nil, fmt.Errorf("project id cannot be empty")
	}

	url := fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/repository/tags", projectId)

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create a http request: %v", err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("request failed on default client: %v", err)
	}
	defer resp.Body.Close()

	var tags []*Tag
	err = json.NewDecoder(resp.Body).Decode(&tags)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json body: %v", err)
	}

	if len(tags) < 1 {
		return nil, fmt.Errorf("no tags found")
	}

	return tags[0], nil
}

type Commit struct {
	ShortId string `json:"short_id"`
	Author  string `json:"author_name"`
	Date    string `json:"committed_date"`
}

func GitCommit(ctx context.Context, projectId string) (*Commit, error) {
	if projectId == "" {
		return nil, fmt.Errorf("project id cannot be empty")
	}

	url := fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/repository/commits", projectId)

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create a http request: %v", err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("request failed on default client: %v", err)
	}
	defer resp.Body.Close()

	var commits []*Commit
	err = json.NewDecoder(resp.Body).Decode(&commits)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json body: %v", err)
	}

	if len(commits) < 1 {
		return nil, fmt.Errorf("no tags found")
	}

	return commits[0], nil
}
