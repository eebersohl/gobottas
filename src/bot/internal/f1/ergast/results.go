package ergast

import "context"

func LastRaceResults(ctx context.Context) ([]*Result, error) {
	panic("implement me")
}

func HistoricalResults(ctx context.Context, season, race int) ([]*Result, error) {
	panic("implement me")
}
