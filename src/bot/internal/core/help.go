package core

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/eebersohl/gobottas/src/bot/internal/discord"
	"gitlab.com/eebersohl/gobottas/src/log"
)

// help is the only built-in module, and it is responsible for displaying basic
// Gobottas usage information any time the "help" command is called for.  It
// implements the Module interface, but due to go import cycle restrictions, it
// must live in the core package.
type help struct {
	cmd              string
	aliases          []string
	long             string
	short            string
	interceptCommand bool
	interceptArgs    bool
	embed            *discordgo.MessageEmbed
	logger           log.Logger
}

// allAliases returns a slice of all command aliases for the Module.  This is
// useful for easily checking if a message is relevant to the module in the
// Intercept function.
func (h *help) allAliases() []string {
	return append([]string{h.cmd}, h.aliases...)
}

// NewHelp creates a new help module.  The help embed is generated up front
// because it will not change.
//
// Params:
//   - fields: A map of command to usage strings to apply to the help embed.
//             Each key value pair is added as a separate field.
//   - long  : The description for the help embed
//   - short : The usage for the help command
//   - intCmd: If true, the module will intercept messages which have a
//             Message.Command value equal to one of the module's aliases.
//   - intArg: If true, the module will intercept messages which have an arg
//             equal to one of the module's aliases.
func NewHelp(fields map[string]string, long, short string, intCmd, intArg bool, logger log.Logger) Module {
	h := &help{
		cmd:              "help",
		aliases:          []string{},
		long:             long,
		short:            short,
		interceptCommand: intCmd,
		interceptArgs:    intArg,
		logger:           logger,
	}

	e := discord.NewEmbed().
		Title("Gobottas usage instructions").
		Color(discord.ColorHelp).
		Description(strings.Replace(h.long, "\n", " ", -1)).
		AddField(h.Name(), h.Usage())

	for name, usage := range fields {
		e.AddField(name, usage)
	}

	e.SortFields()

	h.embed = e.Embed()

	return h
}

// Name returns the name of the module
func (h *help) Name() string {
	return h.cmd
}

// Usage returns a short explanation of the command's function
func (h *help) Usage() string {
	return strings.Replace(h.short, "\n", " ", -1)
}

// Intercept is called on every message that Gobottas sees.  If the parsed command
// is identical to one of the command's aliases, the message response will be set
// to the help embed.
func (h *help) Intercept(msg *Message) error {
	h.logger.Debugf("[%s] beginning interception", h.Name())

	if msg == nil {
		return fmt.Errorf("[%s] cannot intercept a nil message", h.Name())
	}

	if msg.Source == nil {
		return fmt.Errorf("[%s] cannot answer a message with nil source", h.Name())
	}

	if msg.Source.ChannelId == 0 {
		return fmt.Errorf("[%s] cannot respond to a message with an invalid channel id (cid = %d)", h.Name(), msg.Source.ChannelId)
	}

	var intercept bool
	if h.interceptCommand {
		for _, alias := range h.allAliases() {
			if alias == msg.Command {
				intercept = true
			}
		}
	}

	if h.interceptArgs {
		for _, alias := range h.allAliases() {
			for _, arg := range msg.Args {
				if alias == arg {
					intercept = true
				}
			}
		}
	}

	// exit asap if not relevant to this module
	if !intercept {
		h.logger.Debugf("[%s] message is not relevant, ending interception", h.Name())
		return nil
	}

	h.logger.Debugf("[%s] module detected a relevant message (command = %s)", h.Name(), msg.Command)
	msg.Response.Embed = []*discordgo.MessageEmbed{h.embed}
	msg.Response.ChannelId = msg.Source.ChannelId
	h.logger.Debugf("[%s] ending interception", h.Name())
	return nil
}
