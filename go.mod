module gitlab.com/eebersohl/gobottas

go 1.16

require (
	github.com/bwmarrin/discordgo v0.19.0
	github.com/google/go-cmp v0.3.1
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
	go.uber.org/zap v1.19.1 // indirect
)
