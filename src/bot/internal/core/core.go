package core

import (
	"fmt"
	"regexp"
	"strconv"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/eebersohl/gobottas/src/bot/internal/discord"
	"gitlab.com/eebersohl/gobottas/src/log"
)

// Module defines the functionality required for a chunk of functionality
// that gobottas supports
type Module interface {
	// Name returns a string identifier for the module
	Name() string
	// Usage returns a short description of the module functionality for the
	// help module.
	Usage() string
	// Intercept is called on all messages that gobottas sees.  It is imperative
	// that Intercept return as quickly as possible if the message is not
	// relevant to the module.
	Intercept(*Message) error
}

// Registry is the core struct of gobottas
// It holds a list of Modules which define the responses that gobottas
// gives to the various messages it detects in the discord server
type Registry struct {
	// list of Modules that call Intercept on the return Message structs
	modules []Module
	// local directory that stores app relevant data
	local string
	// messages which begin with this character are interpreted as gobottas
	// commands.  Default: '&'.
	commandPrefix uint8
	// the default logger is a no op logger.  pass a different logger with the
	// relevant RegistryOpt
	logger log.Logger
}

type RegistryOpt func(*Registry)

// NewRegistry creates a new Registry based on the options provided.
func NewRegistry(opts ...RegistryOpt) (*Registry, error) {
	r := Registry{
		modules:       []Module{},
		local:         ".",
		commandPrefix: '&',
		logger:        &log.None{},
	}

	for _, opt := range opts {
		opt(&r)
	}

	fields := make(map[string]string)
	for _, mod := range r.modules {
		fields[mod.Name()] = mod.Usage()
	}

	h := NewHelp(fields, _long, _short, true, false, r.logger)
	r.modules = append(r.modules, h)

	return &r, nil
}

// WithModules adds modules to the registry
// NOTE that the builtin help module is included automatically
func WithModules(modules ...Module) RegistryOpt {
	return func(r *Registry) {
		for _, module := range modules {
			r.modules = append(r.modules, module)
		}
	}
}

// WithCommandPrefix sets a different command prefix for gobottas
func WithCommandPrefix(prefix uint8) RegistryOpt {
	return func(r *Registry) {
		r.commandPrefix = prefix
	}
}

// WithLocalDir sets a different directory to store app data
func WithLocalDir(dir string) RegistryOpt {
	return func(r *Registry) {
		r.local = dir
	}
}

// WithLogger adds a logger to the registry
func WithLogger(logger log.Logger) RegistryOpt {
	return func(r *Registry) {
		r.logger = logger
	}
}

// Handler returns a discord message handler that is run on every message that
// gobottas sees.  After all the interceptors run, the Message struct is sent
// through the channel ch.
func (r *Registry) Handler(ch chan<- *Message) func(s *discordgo.Session, m *discordgo.MessageCreate) {
	return func(s *discordgo.Session, m *discordgo.MessageCreate) {
		// always ignore bots
		if m.Author.Bot {
			return
		}

		r.logger.Debugf("parsing message %s", m.Message.ID)
		msg, err := r.parseMessage(m.Message)
		if err != nil {
			r.logger.Errorw("ignoring message due to parse error", "message id", m.ID, "err", err)
			return
		}

		r.logger.Debugf("intercepting message %s", m.Message.ID)
		err = r.runInterceptors(msg)
		if err != nil {
			r.logger.Errorw("ignoring message due to interceptor err", "message id", m.ID, "err", err)
			return
		}

		ch <- msg
	}
}

// parseMessage takes the relevant fields from a discordgo.Message and returns
// a core.Message struct.  The core.Message struct contains the information that
// gobottas needs to respond to a command (notably, it contains the Response struct)
// as well as other metadata.
func (r *Registry) parseMessage(msg *discordgo.Message) (*Message, error) {
	parsed := NewMessage()

	if msg == nil {
		return nil, fmt.Errorf("cannot parse a nil message")
	}

	if msg.Author == nil {
		return nil, fmt.Errorf("discord message has empty author")
	}

	r.logger.Debugf("discord message snowflakes (author = %s, channel = %s)", msg.Author.ID, msg.ChannelID)

	authorId, err := discord.ParseSnowflake(msg.Author.ID)
	if err != nil {
		return nil, fmt.Errorf("discord message has invalid author id %s: %v", msg.Author.ID, err)
	}

	chanId, err := discord.ParseSnowflake(msg.ChannelID)
	if err != nil {
		return nil, fmt.Errorf("discord message has invalid channel id %s: %v", msg.ChannelID, err)
	}

	if authorId == 0 || chanId == 0 {
		return nil, fmt.Errorf("discord message has invalid snowflakes (author = %d, channel = %d)", authorId, chanId)
	}

	parsed.Source = &Source{
		AuthorId:  authorId,
		Author:    msg.Author.Username,
		ChannelId: chanId,
		Content:   msg.Content,
	}

	args, err := tokenize(msg.Content)
	if err != nil {
		return nil, fmt.Errorf("discord message content could not be tokenized: %v", err)
	}

	// check for command prefix
	// otherwise use the default unset zero values for Command and Args
	if args[0][0] == r.commandPrefix {
		parsed.Command = args[0][1:]
		parsed.Args = args[1:]
	}

	return parsed, nil
}

// tokenize uses the dark art of regular expressions to split a string on every
// space while leaving quoted sections intact (including their spaces).
func tokenize(s string) (tokens []string, err error) {
	// parse the string using the dark art of regular expressions
	tokens = regexp.MustCompile(`[^\s"']+|"([^"]*)"|'([^']*)`).FindAllString(s, -1)

	// remove quotes
	for i := range tokens {

		// only call on strings with quotes
		if string(tokens[i][0]) == "\"" {
			tokens[i], err = strconv.Unquote(tokens[i])
			if err != nil {
				return nil, err
			}
		}
	}

	return tokens, nil
}

// runInterceptors runs all of a registry's interceptors on the provided message
// The first interceptor to return a non nil error will terminate execution and
// that error is returned.  An error is also returned on a nil msg pointer.
func (r *Registry) runInterceptors(msg *Message) error {
	if msg == nil {
		return fmt.Errorf("cannot run interceptors on nil message")
	}

	if msg.Response == nil {
		msg.Response = &Response{}
	}

	for _, mod := range r.modules {
		err := mod.Intercept(msg)
		if err != nil {
			return fmt.Errorf("interceptor returned non-nil err: %v", err)
		}
	}
	return nil
}

// Modules returns a list of modules currently defined for the registry
func (r *Registry) Modules() []string {
	var s []string
	for _, mod := range r.modules {
		s = append(s, mod.Name())
	}
	return s
}
