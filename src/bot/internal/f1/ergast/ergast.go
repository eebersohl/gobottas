package ergast

import "time"

// Position holds relevant data about the position that a driver or team hold in
// the relevant championship.
type Position struct {
	// Half points are possible in both championships, so floats are necessary
	Points float64
	// 1 indicates first place
	Position int
	// For drivers, wins is the number of races won
	// For constructors, a "win" is when they score the most points at a race,
	// even if neither of their drivers actually won the race.
	Wins int
	// Driver's names will be "First Last" format
	Name string
}

// Result holds the relevant data from a specific event
type Result struct {
	GridPosition     int
	FinishedPosition int
	Points           float64
	Name             string
	LapsCompleted    int
	Status           string
	Duration         time.Duration
}
