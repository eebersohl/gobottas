package f1

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/eebersohl/gobottas/src/bot/internal/core"
	"gitlab.com/eebersohl/gobottas/src/bot/internal/discord"
	"gitlab.com/eebersohl/gobottas/src/bot/internal/f1/ergast"
	"gitlab.com/eebersohl/gobottas/src/log"
)

type standings struct {
	cmd     string
	aliases []string
	logger  log.Logger
}

func (s *standings) allAliases() []string {
	return append([]string{s.cmd}, s.aliases...)
}

func NewStandings(logger log.Logger) core.Module {
	return &standings{
		cmd:     "standings",
		aliases: []string{},
		logger:  logger,
	}
}

func (s *standings) Name() string {
	return s.cmd
}

func (s *standings) Usage() string {
	return strings.Replace(_standingsShort, "\n", " ", -1)
}

func (s *standings) Intercept(msg *core.Message) error {
	// this is a subcommand, so check the first arg instead of the top level command
	if len(msg.Args) < 1 {
		return nil
	}

	var (
		command   = msg.Args[0]
		intercept = false
	)

	for _, a := range s.allAliases() {
		if a == command {
			intercept = true
		}
	}

	// exit ASAP if this isn't a relevant message
	if !intercept {
		return nil
	}

	championship, year, race, err := parseArgs(msg.Args[1:])
	if err != nil {
		return fmt.Errorf("[%s] %v", s.Name(), err)
	}

	var wcc, wdc []*ergast.Position

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	if championship == "drivers" || championship == "both" {
		wdc, err = ergast.WDCHistorical(ctx, year, race)
		if err != nil {
			return fmt.Errorf("[%s] %v", s.Name(), err)
		}
	}

	if championship == "constructors" || championship == "both" {
		wcc, err = ergast.WCCHistorical(ctx, year, race)
		if err != nil {
			return fmt.Errorf("[%s] %v", s.Name(), err)
		}
	}

	if wdc != nil {
		addResponses(msg, wdc, "Driver's Championship", year, race)
	}

	if wcc != nil {
		addResponses(msg, wcc, "Constructor's Championship", year, race)
	}

	return nil
}

// parseArgs determines whether the args provided are valid for the standings
// module.  This logic is abstracted for readability and ease of testing.
//
// Returns:
// - championship string
//   One of the following: "both" "drivers" "constructors"
//
// - year int
//   A year in the range [1950, present year]
//
// - race int
//   A race number in the range [1, 23] (at time of writing, 23 is the maximum
//   number of races in a single year).
//
// - err error
//   if there is an error, all other returns are zero values
func parseArgs(args []string) (championship string, year int, race int, err error) {
	/*
		This is an enumeration of possible standings command strings:
			&f1 standings
			&f1 standings <year>
			&f1 standings <year> <race>
			&f1 standings <championship>
			&f1 standings <championship> <year>
			&f1 standings <championship> <year> <race>
		Note that year must be parsable into an integer in the range [1950, present
		year], and race must be parsable into an integer in the range [1, 23].
		Championship may be any of these strings: "drivers, wdc, constructors, wcc"
	*/

	if len(args) == 0 {
		championship = "both"
		return
	}

	if len(args) == 1 {
		if num, ok := validYear(args[0]); ok {
			championship = "both"
			year = num
			return
		}

		if s, ok := validChampionship(args[0]); ok {
			championship = s
			return
		}

		return "", 0, 0, fmt.Errorf("found one arg that is not a valid year or championship")
	}

	if len(args) == 2 {
		if n, ok := validYear(args[0]); ok {
			if m, ok := validRace(args[1]); ok {
				year = n
				race = m
				championship = "both"
				return
			}
			return "", 0, 0, fmt.Errorf("second arg after year is not a valid race number")
		}

		if s, ok := validChampionship(args[0]); ok {
			if num, ok := validYear(args[1]); ok {
				year = num
				championship = s
				return
			}
			return "", 0, 0, fmt.Errorf("second arg after championship is not valid year")
		}
		return "", 0, 0, fmt.Errorf("found two args that are not valid")
	}

	if len(args) == 3 {
		if s, ok := validChampionship(args[0]); ok {
			if n, ok := validYear(args[1]); ok {
				if m, ok := validRace(args[2]); ok {
					championship = s
					year = n
					race = m
					return
				}
				return "", 0, 0, fmt.Errorf("arg three was not a valid race string")
			}
			return "", 0, 0, fmt.Errorf("arg two was not a valid year")
		}
		return "", 0, 0, fmt.Errorf("arg one was not a valid championship")
	}
	return "", 0, 0, fmt.Errorf("too many args for interceptor to parse")
}

func validChampionship(s string) (string, bool) {
	if s == "wdc" || s == "drivers" {
		return "drivers", true
	}

	if s == "wcc" || s == "constructors" {
		return "constructors", true
	}

	return "", false
}

func validYear(s string) (int, bool) {
	num, err := strconv.Atoi(s)
	if err != nil {
		return 0, false
	}

	if num >= 1950 && num <= time.Now().Year() {
		return num, true
	}

	return 0, false
}

func validRace(s string) (int, bool) {
	num, err := strconv.Atoi(s)
	if err != nil {
		return 0, false
	}

	if num >= 1 && num <= 23 {
		return num, true
	}

	return 0, false
}

func addResponses(msg *core.Message, pos []*ergast.Position, championship string, year int, race int) {
	e := discord.NewEmbed().
		Title(fmt.Sprintf("%s Standings", championship)).
		Color(discord.ColorF1)

	var desc string
	if year != 0 && race != 0 {
		desc = fmt.Sprintf("These were the standings after race **%d** of the **%d** season.", race, year)
	} else if year != 0 && race == 0 {
		desc = fmt.Sprintf("These were the standings at the end of the **%d** season.", year)
	} else if year == 0 && race != 0 {
		desc = fmt.Sprintf("These were the standings after race **%d** of the current season.", race)
	}

	if desc != "" {
		e.Description(desc)
	}

	var text []string
	for _, p := range pos {
		name := fmt.Sprintf("%02d %s", p.Position, p.Name)
		stats := fmt.Sprintf("%.1f points, %d wins", p.Points, p.Wins)

		e.AddField(name, stats)
		text = append(text, fmt.Sprintf("%s (%s)", name, stats))
	}

	msg.Response.Embed = append(msg.Response.Embed, e.Embed())
	msg.Response.Text = text
}
