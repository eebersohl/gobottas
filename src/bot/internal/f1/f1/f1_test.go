package f1

import (
	"strings"
	"testing"

	"gitlab.com/eebersohl/gobottas/src/log"
)

func TestF1_Name(t *testing.T) {
	f := NewF1(&log.None{})

	if got := f.Name(); got != "f1" {
		t.Errorf("f1 module did not return the correct name (got %s, want f1)", got)
	}
}

func TestF1_Usage(t *testing.T) {
	f := NewF1(&log.None{})
	want := strings.Replace(_f1Short, "\n", " ", -1)

	if got := f.Usage(); got != want {
		t.Errorf("got usage != want usage (got = %s, want = %s)", got, want)
	}
}

func TestF1_Intercept(t *testing.T) {
	// todo(ee): test this once all interceptors are implemented
}
