package zap

import (
	"gitlab.com/eebersohl/gobottas/src/log"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// Ensure that the zap SugaredLogger implements the Logger interface
var _ log.Logger = &SugaredLogger{}

// SugaredLogger wraps the zap struct of the same name in order to provide extra
// functionality required by the log.Logger interface.
type SugaredLogger struct {
	*zap.SugaredLogger
}

func (sl *SugaredLogger) With(keysAndValues ...interface{}) log.Logger {
	return &SugaredLogger{
		SugaredLogger: sl.SugaredLogger.With(keysAndValues...),
	}
}

// NewDevelopmentLogger returns a SugaredLogger with a properly configured
// zap logger under the hood.  For more about zap loggers, see
// https://github.com/uber-go/zap
func NewDevelopmentLogger() *SugaredLogger {
	encoderCfg := zap.NewDevelopmentEncoderConfig()
	encoderCfg.EncodeTime = zapcore.ISO8601TimeEncoder

	logCfg := zap.NewDevelopmentConfig()
	logCfg.EncoderConfig = encoderCfg

	logger, err := logCfg.Build()
	if err != nil {
		panic(err)
	}

	return &SugaredLogger{SugaredLogger: logger.Sugar()}
}

// NewProductionLogger is unlikely to ever be used in the project, but I have
// included it here for completeness.
func NewProductionLogger() *SugaredLogger {
	encoderCfg := zap.NewProductionEncoderConfig()
	encoderCfg.EncodeTime = zapcore.ISO8601TimeEncoder

	logCfg := zap.NewProductionConfig()
	logCfg.EncoderConfig = encoderCfg

	logger, err := logCfg.Build()
	if err != nil {
		panic(err)
	}

	return &SugaredLogger{SugaredLogger: logger.Sugar()}
}
