package discord

import "testing"

/*
Test Cases:
- non numeric string
- negative number
- normal case
- overflow
*/
func TestParseSnowflake(t *testing.T) {
	tests := []struct {
		name    string
		flake   string
		want    Snowflake
		wantErr bool
	}{
		{name: "non numeral", flake: "snowflake", want: 0, wantErr: true},
		{name: "negative", flake: "-175928847299117063", want: 0, wantErr: true},
		{name: "overflow", flake: "175928847299117063175928847299117063", want: 0, wantErr: true},
		{name: "normal", flake: "175928847299117063", want: 175928847299117063, wantErr: false},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			got, err := ParseSnowflake(test.flake)
			if (err != nil) != test.wantErr {
				t.Errorf("bad err state (err = %v, want = %t)", err, test.wantErr)
			}

			if err == nil && !test.wantErr {
				if got != test.want {
					t.Errorf("got != want (got = %d, want = %d)", got, test.want)
				}
			}
		})
	}
}
