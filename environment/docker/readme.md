# Armv7 problems
Building this image on ARMv7 requires some extra dependencies.  See these discussions:
* https://github.com/docker/for-linux/issues/1196
* https://wiki.alpinelinux.org/wiki/Release_Notes_for_Alpine_3.13.0#time64_requirements