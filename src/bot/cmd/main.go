package main

import (
	"flag"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/eebersohl/gobottas/src/bot/internal/core"
	"gitlab.com/eebersohl/gobottas/src/bot/internal/f1/f1"
	"gitlab.com/eebersohl/gobottas/src/bot/internal/repo"
	"gitlab.com/eebersohl/gobottas/src/log"
	"gitlab.com/eebersohl/gobottas/src/log/zap"
	"os"
)

func main() {
	var (
		_commandPrefix string
		_localPath     string
		_production    bool
		_v             bool
	)

	flag.StringVar(&_commandPrefix, "prefix", "&", "set the one-character command prefix")
	flag.StringVar(&_localPath, "dir", ".", "set the local dir where files can be stored")
	flag.BoolVar(&_production, "production", false, "set this value to true to use production logging")
	flag.BoolVar(&_v, "v", false, "set verbose logging mode")
	flag.Parse()

	logger := zap.NewDevelopmentLogger()
	if _production {
		logger = zap.NewProductionLogger()
	}

	if len(_commandPrefix) > 1 {
		logger.Fatalw("command prefix must have 1 and only 1 character", "prefix", _commandPrefix)
	}

	// the default non-verbose setting does not send loggers to any module
	// if verbose is set, then send a real logger to the registry and its modules
	var moduleLogger log.Logger = &log.None{}
	if _v {
		logger.Info("Gobottas is running in verbose logging mode")
		moduleLogger = logger
	}

	// create the registry with all enabled modules
	registry, err := core.NewRegistry(core.WithModules(
		repo.NewRepo(os.Getenv("PROJECT"), moduleLogger),
		f1.NewF1(moduleLogger),
	), core.WithLogger(moduleLogger))
	if err != nil {
		logger.Fatalw("failed to create a new registry", "err", err)
	}

	msgChan := make(chan *core.Message)

	// connect to server
	discord, err := discordgo.New("Bot " + os.Getenv("AUTH"))
	if err != nil {
		logger.Fatalw("failed to create discord client", "err", err)
	}

	// add handler
	discord.AddHandler(registry.Handler(msgChan))

	// connect to discord (uses websocket)
	err = discord.Open()
	if err != nil {
		logger.Fatalw("failed to open websocket conn to discord", "err", err)
	}
	defer discord.Close()

	logger.Info("Gobottas initialized")

	// send messages to discord according to the following hierarchy:
	// - if the response embed is defined, send it
	// - if the response text is defined, send it
	for msg := range msgChan {
		logger.Debugw("message received after intercepting", "response channel", msg.Response.ChannelId)

		// send the response if there is a valid response to be sent
		if msg.Response != nil {
			rChan := msg.Response.ChannelId.String()

			// if there are any embeds, send them
			if msg.Response.Embed != nil {
				for _, embed := range msg.Response.Embed {
					_, err = discord.ChannelMessageSendEmbed(rChan, embed)
					if err != nil {
						logger.Errorw("failed to send message response embed", "destination channel", rChan, "err", err)
					}
				}
				continue
			}

			// if there are any text responses, send them
			if msg.Response.Text != nil {
				for _, resp := range msg.Response.Text {
					_, err = discord.ChannelMessageSend(rChan, resp)
					if err != nil {
						logger.Errorw("failed to send message response text",
							"destination channel", rChan,
							"response text", msg.Response.Text,
							"err", err)
					}
				}
				continue
			}
		}
	}
}
