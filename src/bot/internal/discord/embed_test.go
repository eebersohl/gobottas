package discord

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"github.com/google/go-cmp/cmp"
	"strings"
	"testing"
	"time"
)

func TestEmbed(t *testing.T) {
	e := NewEmbed()

	if e.Embed() == nil {
		t.Errorf("embed func returned a nil pointer")
	}
}

/*
Test Cases:
- author name char limit
- total char limit
*/
func TestEmbed_Author(t *testing.T) {
	var (
		_256 = strings.Repeat("x", 256)
		_257 = strings.Repeat("x", 257)
	)
	tests := []struct {
		name         string
		base         *Embed
		url          string
		authorName   string
		iconUrl      string
		proxyIconUrl string
		want         *discordgo.MessageEmbedAuthor
	}{
		{name: "author name 256", base: NewEmbed(), url: "some-url", authorName: _256, iconUrl: "some-other-url", proxyIconUrl: "some-other-other-url", want: &discordgo.MessageEmbedAuthor{
			URL:          "some-url",
			Name:         _256,
			IconURL:      "some-other-url",
			ProxyIconURL: "some-other-other-url",
		}},
		{name: "author name 257", base: NewEmbed(), url: "some-url", authorName: _257, iconUrl: "some-other-url", proxyIconUrl: "some-other-other-url", want: &discordgo.MessageEmbedAuthor{
			URL:          "some-url",
			Name:         strings.Repeat("x", 253) + "...",
			IconURL:      "some-other-url",
			ProxyIconURL: "some-other-other-url",
		}},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Logf("%d", test.base.totalChars)
			got := test.base.Author(test.url, test.authorName, test.iconUrl, test.proxyIconUrl).Embed()
			if eq := cmp.Diff(got.Author, test.want); eq != "" {
				t.Errorf(eq)
			}
		})
	}
}

func TestEmbed_URL(t *testing.T) {
	e := NewEmbed().URL("url").Embed()

	if e.URL != "url" {
		t.Errorf("url failed to set the url")
	}
}

/*
Test Cases
- 256 length title
- 257 length title
*/
func TestEmbed_Title(t *testing.T) {
	var (
		_256 = strings.Repeat("x", 256)
		_257 = strings.Repeat("x", 257)
	)
	tests := []struct {
		name  string
		base  *Embed
		title string
		want  string
	}{
		{name: "256 length", base: NewEmbed(), title: _256, want: _256},
		{name: "257 length", base: NewEmbed(), title: _257, want: strings.Repeat("x", 253) + "..."},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			e := test.base.Title(test.title).Embed()
			if e.Title != test.want {
				t.Errorf("got title != want title (got = %s, want = %s)", e.Title, test.want)
			}
		})
	}
}

/*
Test Cases:
- 2048 chars
- 2049 chars
*/
func TestEmbed_Description(t *testing.T) {
	var (
		_2048 = strings.Repeat("x", 2048)
		_2049 = strings.Repeat("x", 2049)
	)

	tests := []struct {
		name string
		base *Embed
		desc string
		want string
	}{
		{name: "2048 chars", base: NewEmbed(), desc: _2048, want: _2048},
		{name: "2049 chars", base: NewEmbed(), desc: _2049, want: strings.Repeat("x", 2045) + "..."},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			e := test.base.Description(test.desc).Embed()
			if e.Description != test.want {
				t.Errorf("got != want (got = %s, want = %s)", e.Description, test.want)
			}
		})
	}
}

/*
Test Cases:
- field normal
- field name chars
- field val chars
- empty name
- empty val
- too many fields
*/
func TestEmbed_AddField(t *testing.T) {
	var (
		_256      = strings.Repeat("x", 256)
		_257      = strings.Repeat("x", 257)
		_1024     = strings.Repeat("x", 1024)
		_1025     = strings.Repeat("x", 1025)
		_24Fields = NewEmbed()
		_25Fields = NewEmbed()
	)

	for i := 0; i < 24; i++ {
		_24Fields.AddField(fmt.Sprintf("%d", i), "value")
		_25Fields.AddField(fmt.Sprintf("%d", i), "value")
	}
	_25Fields.AddField("25", "value")

	tests := []struct {
		name       string
		base       *Embed
		fieldName  string
		fieldVal   string
		wantFields int
		want       *discordgo.MessageEmbedField
	}{
		{name: "field max chars", base: NewEmbed(), fieldName: _256, fieldVal: _1024, wantFields: 1, want: &discordgo.MessageEmbedField{
			Name:   _256,
			Value:  _1024,
			Inline: false,
		}},
		{name: "field name 257", base: NewEmbed(), fieldName: _257, fieldVal: _1024, wantFields: 1, want: &discordgo.MessageEmbedField{
			Name:   strings.Repeat("x", 253) + "...",
			Value:  _1024,
			Inline: false,
		}},
		{name: "field val 1025", base: NewEmbed(), fieldName: _256, fieldVal: _1025, wantFields: 1, want: &discordgo.MessageEmbedField{
			Name:   _256,
			Value:  strings.Repeat("x", 1021) + "...",
			Inline: false,
		}},
		{name: "empty name", base: NewEmbed(), fieldName: "", fieldVal: _1024, wantFields: 0, want: nil},
		{name: "empty val", base: NewEmbed(), fieldName: _256, fieldVal: "", wantFields: 0, want: nil},
		{name: "max fields", base: _24Fields, fieldName: _256, fieldVal: _1024, wantFields: 25, want: &discordgo.MessageEmbedField{
			Name:   _256,
			Value:  _1024,
			Inline: false,
		}},
		{name: "field overflow", base: _25Fields, fieldName: _256, fieldVal: _1024, wantFields: 25, want: &discordgo.MessageEmbedField{
			Name:   "25",
			Value:  "value",
			Inline: false,
		}},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			e := test.base.AddField(test.fieldName, test.fieldVal).Embed()
			if len(e.Fields) != test.wantFields {
				t.Errorf("got num fields != want num fields (got = %d, want = %d)", len(e.Fields), test.wantFields)
			}

			if len(e.Fields) > 0 {
				if eq := cmp.Diff(e.Fields[len(e.Fields)-1], test.want); eq != "" {
					t.Errorf(eq)
				}
			}
		})
	}
}

func TestEmbed_AddInlineField(t *testing.T) {
	var (
		_256      = strings.Repeat("x", 256)
		_257      = strings.Repeat("x", 257)
		_1024     = strings.Repeat("x", 1024)
		_1025     = strings.Repeat("x", 1025)
		_24Fields = NewEmbed()
		_25Fields = NewEmbed()
	)

	for i := 0; i < 24; i++ {
		_24Fields.AddInlineField(fmt.Sprintf("%d", i), "value")
		_25Fields.AddInlineField(fmt.Sprintf("%d", i), "value")
	}
	_25Fields.AddInlineField("25", "value")

	tests := []struct {
		name       string
		base       *Embed
		fieldName  string
		fieldVal   string
		wantFields int
		want       *discordgo.MessageEmbedField
	}{
		{name: "field max chars", base: NewEmbed(), fieldName: _256, fieldVal: _1024, wantFields: 1, want: &discordgo.MessageEmbedField{
			Name:   _256,
			Value:  _1024,
			Inline: true,
		}},
		{name: "field name 257", base: NewEmbed(), fieldName: _257, fieldVal: _1024, wantFields: 1, want: &discordgo.MessageEmbedField{
			Name:   strings.Repeat("x", 253) + "...",
			Value:  _1024,
			Inline: true,
		}},
		{name: "field val 1025", base: NewEmbed(), fieldName: _256, fieldVal: _1025, wantFields: 1, want: &discordgo.MessageEmbedField{
			Name:   _256,
			Value:  strings.Repeat("x", 1021) + "...",
			Inline: true,
		}},
		{name: "empty name", base: NewEmbed(), fieldName: "", fieldVal: _1024, wantFields: 0, want: nil},
		{name: "empty val", base: NewEmbed(), fieldName: _256, fieldVal: "", wantFields: 0, want: nil},
		{name: "max fields", base: _24Fields, fieldName: _256, fieldVal: _1024, wantFields: 25, want: &discordgo.MessageEmbedField{
			Name:   _256,
			Value:  _1024,
			Inline: true,
		}},
		{name: "field overflow", base: _25Fields, fieldName: _256, fieldVal: _1024, wantFields: 25, want: &discordgo.MessageEmbedField{
			Name:   "25",
			Value:  "value",
			Inline: true,
		}},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			e := test.base.AddInlineField(test.fieldName, test.fieldVal).Embed()
			if len(e.Fields) != test.wantFields {
				t.Errorf("got num fields != want num fields (got = %d, want = %d)", len(e.Fields), test.wantFields)
			}

			if len(e.Fields) > 0 {
				if eq := cmp.Diff(e.Fields[len(e.Fields)-1], test.want); eq != "" {
					t.Errorf(eq)
				}
			}
		})
	}
}

func TestEmbed_SortFields(t *testing.T) {
	e := NewEmbed().
		AddField("c", "c-value").
		AddField("b", "b-value").
		AddField("a", "a-value").
		SortFields().Embed()

	want := []*discordgo.MessageEmbedField{
		{
			Name:   "a",
			Value:  "a-value",
			Inline: false,
		},
		{
			Name:   "b",
			Value:  "b-value",
			Inline: false,
		},
		{
			Name:   "c",
			Value:  "c-value",
			Inline: false,
		},
	}

	if eq := cmp.Diff(e.Fields, want); eq != "" {
		t.Errorf(eq)
	}
}

func TestEmbed_Timestamp(t *testing.T) {
	testTime := time.Now()

	e := NewEmbed().Timestamp(testTime).Embed()

	if e.Timestamp != testTime.Format(time.RFC3339) {
		t.Errorf("got time != want time (got = %s, want = %s)", e.Timestamp, testTime.String())
	}
}

func TestEmbed_Color(t *testing.T) {
	e := NewEmbed().Color(ColorError).Embed()
	if e.Color != ColorError {
		t.Errorf("embed color did not set the color (got = %d, want = %d)", e.Color, ColorError)
	}
}

/*
Test Cases:
- 2048 footer
- 2049 footer
*/
func TestEmbed_Footer(t *testing.T) {
	var (
		_2048 = strings.Repeat("x", 2048)
		_2049 = strings.Repeat("x", 2049)
	)

	tests := []struct {
		name        string
		base        *Embed
		footerText  string
		footerUrl   string
		footerProxy string
		want        *discordgo.MessageEmbedFooter
	}{
		{name: "2048 chars", base: NewEmbed(), footerText: _2048, footerUrl: "url", footerProxy: "proxy-url", want: &discordgo.MessageEmbedFooter{
			Text:         _2048,
			IconURL:      "url",
			ProxyIconURL: "proxy-url",
		}},
		{name: "2049 chars", base: NewEmbed(), footerText: _2049, footerUrl: "url", footerProxy: "proxy-url", want: &discordgo.MessageEmbedFooter{
			Text:         strings.Repeat("x", 2045) + "...",
			IconURL:      "url",
			ProxyIconURL: "proxy-url",
		}},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			e := test.base.Footer(test.footerText, test.footerUrl, test.footerProxy).Embed()
			if eq := cmp.Diff(e.Footer, test.want); eq != "" {
				t.Error(eq)
			}
		})
	}
}

func TestEmbed_TotalChars(t *testing.T) {
	full := func() *Embed {
		e := NewEmbed()
		for i := 0; i < 10; i++ {
			e.AddField(strings.Repeat("x", 200), strings.Repeat("x", 390))
		}
		return e
	}

	tests := []struct {
		name      string
		e         *Embed
		wantChars int
	}{
		{name: "check sum", e: NewEmbed().
			Title(strings.Repeat("x", 200)).
			Description(strings.Repeat("x", 200)).
			AddField(strings.Repeat("x", 200), strings.Repeat("x", 200)).
			Footer(strings.Repeat("x", 200), "", "").
			Author("", strings.Repeat("x", 200), "", ""), wantChars: 1200},
		{name: "title overflow", e: full().Title(strings.Repeat("x", 200)), wantChars: 5900},
		{name: "desc overflow", e: full().Description(strings.Repeat("x", 200)), wantChars: 5900},
		{name: "field name overflow", e: full().AddField(strings.Repeat("x", 200), "x"), wantChars: 5900},
		{name: "field val overflow", e: full().AddField("x", strings.Repeat("x", 200)), wantChars: 5900},
		{name: "inline field name overflow", e: full().AddInlineField(strings.Repeat("x", 200), "x"), wantChars: 5900},
		{name: "inline field val overflow", e: full().AddInlineField("x", strings.Repeat("x", 200)), wantChars: 5900},
		{name: "footer overflow", e: full().Footer(strings.Repeat("x", 200), "", ""), wantChars: 5900},
		{name: "author name overflow", e: full().Author("", strings.Repeat("x", 200), "", ""), wantChars: 5900},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			if test.e.totalChars != test.wantChars {
				t.Errorf("got chars != want chars (got = %d, want = %d)", test.e.totalChars, test.wantChars)
			}
		})
	}
}
