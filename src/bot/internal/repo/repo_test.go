package repo

import (
	"testing"

	"gitlab.com/eebersohl/gobottas/src/bot/internal/core"
	"gitlab.com/eebersohl/gobottas/src/bot/internal/discord"
	"gitlab.com/eebersohl/gobottas/src/log"
)

func TestRepo_Name(t *testing.T) {
	r := NewRepo("", &log.None{})

	if got := r.Name(); got != "repo" {
		t.Errorf("repo module did not return the correct name (got %s, want repo)", got)
	}
}

func TestRepo_Usage_New(t *testing.T) {
	// todo(ee)
}

/*
Test Cases
- message is nil
- message has no source
- empty command
- repo command
- repository command
- close command (repos)
*/
func TestRepo_Intercept(t *testing.T) {
	cId, _ := discord.ParseSnowflake("175928847299117063")

	tests := []struct {
		name      string
		in        *core.Message
		wantEmbed bool
		wantText  bool
		wantTitle string
		wantErr   bool
	}{
		{name: "nil message", in: nil, wantErr: true},
		{name: "nil source", in: &core.Message{Source: nil, Command: "repo"}, wantErr: true},
		{name: "empty command", in: &core.Message{
			Source: &core.Source{
				ChannelId: cId,
			},
			Command: "",
		}},
		{name: "repo", in: &core.Message{
			Source: &core.Source{
				ChannelId: cId,
			},
			Command: "repo",
		}, wantEmbed: true, wantText: true, wantTitle: "Gobottas is on gitlab"},
		{name: "repository", in: &core.Message{
			Source: &core.Source{
				ChannelId: cId,
			},
			Command: "repository",
		}, wantEmbed: true, wantText: true, wantTitle: "Gobottas is on gitlab"},
		{name: "repos", in: &core.Message{
			Source: &core.Source{
				ChannelId: cId,
			},
			Command: "repos",
		}},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			r := NewRepo("", &log.None{})

			err := r.Intercept(test.in)
			if (err != nil) != test.wantErr {
				t.Errorf("bad err state (err = %v, wantErr = %t)", err, test.wantErr)
			}

			if err == nil && !test.wantErr {
				embed := test.in.Response != nil && test.in.Response.Embed != nil
				text := test.in.Response != nil && test.in.Response.Text != nil
				var title string
				if embed {
					title = test.in.Response.Embed[0].Title
				}

				if embed != test.wantEmbed {
					t.Errorf("bad wantEmbed state (got = %t, want = %t)", embed, test.wantEmbed)
				}

				if text != test.wantText {
					t.Errorf("bad wantText state (got = %t, want = %t)", text, test.wantText)
				}

				if title != test.wantTitle {
					t.Errorf("got title != want title (got = %s, want = %s)", title, test.wantTitle)
				}
			}
		})
	}
}
