package discord

import (
	"sort"
	"time"

	"github.com/bwmarrin/discordgo"
)

const (
	ColorError = 0xD0021B
	ColorHelp  = 0xF7FA00
	ColorF1    = 0x1e7cd9
)

// discord embeds have the following size limitations
const (
	_titleCharLimit       = 256
	_descriptionCharLimit = 2048
	_numFieldsLimit       = 25
	_fieldNameCharLimit   = 256
	_fieldValueCharLimit  = 1024
	_footerCharLimit      = 2048
	_authorNameCharLimit  = 256

	// total = title + description + all field names + all field values + footer + author
	_totalCharLimit = 6000
)

type Embed struct {
	embed      *discordgo.MessageEmbed
	totalChars int
}

// NewEmbed returns an empty Embed struct
func NewEmbed() *Embed {
	e := Embed{
		embed:      &discordgo.MessageEmbed{},
		totalChars: 0,
	}
	return &e
}

// Embed returns the underlying discordgo.MessageEmbed
func (e *Embed) Embed() *discordgo.MessageEmbed {
	return e.embed
}

// Author adds an author to the embed.  If name is longer than the name character
// limit, it is truncated with an ellipsis.  If adding the name would push the
// embed above the character limit, nothing is changed and the embed is returned.
func (e *Embed) Author(url, name, iconUrl, proxyIconUrl string) *Embed {
	if len(name) > _authorNameCharLimit {
		name = name[:_authorNameCharLimit-3] + "..."
	}

	if e.totalChars+len(name) > _totalCharLimit {
		return e
	}

	e.embed.Author = &discordgo.MessageEmbedAuthor{
		URL:          url,
		Name:         name,
		IconURL:      iconUrl,
		ProxyIconURL: proxyIconUrl,
	}

	e.totalChars += len(name)

	return e
}

// URL adds a URL to the embed
// url length does not contribute toward total char limit
func (e *Embed) URL(s string) *Embed {
	e.embed.URL = s
	return e
}

// Title adds a title to the embed.  If s is longer than the title character
// limit, the title is automatically truncated with an ellipsis.  If adding the
// title would push the embed over the character limit, the title will not be
// added and the original embed will be returned.
func (e *Embed) Title(s string) *Embed {
	if len(s) > _titleCharLimit {
		s = s[:_titleCharLimit-3] + "..."
	}

	// cannot add any more characters, return the original embed to avoid panics
	if e.totalChars+len(s) > _totalCharLimit {
		return e
	}

	e.embed.Title = s
	e.totalChars += len(s)

	return e
}

// Description adds a top level description to the embed.  If s is longer than
// the description character limit, it will automatically be truncated with an
// ellipsis.  If adding the description would push the embed over the character
// limit, the original embed will be returned without adding the description.
func (e *Embed) Description(s string) *Embed {
	if len(s) > _descriptionCharLimit {
		s = s[:_descriptionCharLimit-3] + "..."
	}

	if e.totalChars+len(s) > _totalCharLimit {
		return e
	}

	e.embed.Description = s
	e.totalChars += len(s)

	return e
}

// AddField adds a normal (non-inlined) field to the embed.  Fields will be
// displayed in the order that they are added.  If name or value are longer than
// their respective character limits, they are truncated with an ellipsis.
// If one or more of the following error conditions is met, the Embed is
// returned with no changes:
// -  name is an empty string
// -  value is an empty string
// -  the embed is already at the field limit
// -  adding the field would push the embed over the character limit
func (e *Embed) AddField(name, value string) *Embed {
	if len(name) > _fieldNameCharLimit {
		name = name[:_fieldNameCharLimit-3] + "..."
	}

	if len(value) > _fieldValueCharLimit {
		value = value[:_fieldValueCharLimit-3] + "..."
	}

	if name == "" || value == "" {
		return e
	}

	if len(e.embed.Fields) >= _numFieldsLimit {
		return e
	}

	if e.totalChars+len(name)+len(value) > _totalCharLimit {
		return e
	}

	e.embed.Fields = append(e.embed.Fields, &discordgo.MessageEmbedField{
		Name:   name,
		Value:  value,
		Inline: false,
	})

	e.totalChars = e.totalChars + len(name) + len(value)

	return e
}

// SortFields sorts the slice containing fields in place using sort.Slice.
// It exists because go maps iterate over their keys randomly.
func (e *Embed) SortFields() *Embed {
	sort.Slice(e.embed.Fields, func(i, j int) bool {
		return e.embed.Fields[i].Name < e.embed.Fields[j].Name
	})
	return e
}

// AddInlineField adds a normal (non-inlined) field to the embed.  Fields will be
// displayed in the order that they are added.  If name or value are longer than
// their respective character limits, they are truncated with an ellipsis.
// If one or more of the following error conditions is met, the Embed is
// returned with no changes:
// -  name is an empty string
// -  value is an empty string
// -  the embed is already at the field limit
// -  adding the field would push the embed over the character limit
func (e *Embed) AddInlineField(name, value string) *Embed {
	if len(name) > _fieldNameCharLimit {
		name = name[:_fieldNameCharLimit-3] + "..."
	}

	if len(value) > _fieldValueCharLimit {
		value = value[:_fieldValueCharLimit-3] + "..."
	}

	if name == "" || value == "" {
		return e
	}

	if len(e.embed.Fields) >= _numFieldsLimit {
		return e
	}

	if e.totalChars+len(name)+len(value) > _totalCharLimit {
		return e
	}

	e.embed.Fields = append(e.embed.Fields, &discordgo.MessageEmbedField{
		Name:   name,
		Value:  value,
		Inline: true,
	})

	e.totalChars = e.totalChars + len(name) + len(value)

	return e
}

// Timestamp adds a custom timestamp to the embed's footer
func (e *Embed) Timestamp(t time.Time) *Embed {
	e.embed.Timestamp = t.Format(time.RFC3339)
	return e
}

// Color sets the color of the embed
func (e *Embed) Color(c int) *Embed {
	e.embed.Color = c
	return e
}

// Footer sets the content of the embed's footer.  If text has more characters
// than the footer character limit, it is truncated with an ellipsis.  If adding
// text would push the embed over the character limit, nothing is added and the
// original embed is returned.
func (e *Embed) Footer(text, iconUrl, proxyIconUrl string) *Embed {
	if len(text) > _footerCharLimit {
		text = text[:_footerCharLimit-3] + "..."
	}

	if e.totalChars+len(text) > _totalCharLimit {
		return e
	}

	e.embed.Footer = &discordgo.MessageEmbedFooter{
		Text:         text,
		IconURL:      iconUrl,
		ProxyIconURL: proxyIconUrl,
	}

	e.totalChars += len(text)

	return e
}
