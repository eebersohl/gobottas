package log

// The Logger interface supplies the functions which are to be used for logging
// throughout the project.  It was developed with the uber/zap project in mind.
type Logger interface {
	// Info writes basic logs (usually strings but any variables are allowed in
	// principle) at the info level without additional context or formatting.
	Info(args ...interface{})
	// Debug writes basic logs (usually strings but any variables are allowed in
	// principle) at the debug level without additional context or formatting.
	Debug(args ...interface{})
	// Error writes basic logs (usually strings but any variables are allowed in
	// principle) at the error level without additional context or formatting.
	Error(args ...interface{})
	// Fatal writes basic logs (usually strings but any variables are allowed in
	// principle) at the fatal level without additional context or formatting.
	Fatal(args ...interface{})

	// Infof writes formatted string logs (exactly like fmt.Printf) at the info
	// level.
	Infof(template string, args ...interface{})
	// Debugf writes formatted string logs (exactly like fmt.Printf) at the
	// debug level.
	Debugf(template string, args ...interface{})
	// Errorf writes formatted string logs (exactly like fmt.Printf) at the
	// error level.
	Errorf(template string, args ...interface{})
	// Fatalf writes formatted string logs (exactly like fmt.Printf) at the
	// fatal level.
	Fatalf(template string, args ...interface{})

	// Infow writes a string message followed by an arbitrary number of key/value
	// pairs which contain extra context for the message.  It is assumed that
	// the first extra argument is the first key and the next argument is the
	// first value.
	Infow(msg string, keysAndValues ...interface{})
	// Debugw writes a string message followed by an arbitrary number of key/value
	// pairs which contain extra context for the message.  It is assumed that
	// the first extra argument is the first key and the next argument is the
	// first value.
	Debugw(msg string, keysAndValues ...interface{})
	// Errorw writes a string message followed by an arbitrary number of key/value
	// pairs which contain extra context for the message.  It is assumed that
	// the first extra argument is the first key and the next argument is the
	// first value.
	Errorw(msg string, keysAndValues ...interface{})
	// Fatalw writes a string message followed by an arbitrary number of key/value
	// pairs which contain extra context for the message.  It is assumed that
	// the first extra argument is the first key and the next argument is the
	// first value.
	Fatalw(msg string, keysAndValues ...interface{})

	// With returns a new Logger that has the keysAndValues applied to every
	// log entry.
	With(keysAndValues ...interface{}) Logger
}
